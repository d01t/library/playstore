ARG VERSION=latest

FROM node:${VERSION}

COPY install.sh /root/

RUN /bin/bash /root/install.sh
